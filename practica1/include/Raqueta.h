// Raqueta.h: interface for the Raqueta class.
//
//////////////////////////////////////////////////////////////////////

#include "Plano.h"
#include "Vector2D.h"
#include <iostream>
class Raqueta : public Plano  
{
public:
	Vector2D velocidad;

	Raqueta();
	virtual ~Raqueta();
	int pulso;
	void RaquetaPulsante(float );
	bool parado;
 	int tiempoParado;
	void Mueve(float t);
};
