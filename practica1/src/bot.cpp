#include "../include/DatosMemCompartida.h"
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>

int main() {	
	void *org;
	DatosMemCompartida *datos;
	int fd_memoria;
	char *p;
	struct stat bstat;
	if((fd_memoria=open("/tmp/bot.txt", O_CREAT|O_RDWR,0777))<0)
		perror("Error al abrir fichero");	
	if (fstat(fd_memoria, &bstat)<0) {
	perror("Error en fstat del fichero"); close(fd_memoria);
   		return -1;
}
	//Proyeccion del fichero	
if((org=mmap((caddr_t) 0,bstat.st_size, PROT_READ|PROT_WRITE ,MAP_SHARED,fd_memoria,0))==MAP_FAILED){ 
   		perror("Error en la proyeccion del fichero"); 
   		close(fd_memoria);
		return -1;
   	}
	close(fd_memoria);//se cierra el fichero
	datos=static_cast<DatosMemCompartida*>(org);	
	while(1)
	{
		if ((datos->esfera.centro.y)>=datos->raqueta1.y1)
		{
			datos->accion=1;
		}
		else if((datos->esfera.centro.y)<=datos->raqueta1.y2)
                {
			datos->accion=-1;
		}
		else datos->accion=0;
	}		
}
